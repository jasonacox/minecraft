# CHANGELOG for minecraft

This file is used to list changes made in each version of minecraft.

## 0.2.0:

* Initial release of minecraft

## 0.2.8:

* Added dependencies to install Java and Screen
* Added js files for jsawk (used for minecraft version upgrades)

- - - 
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
